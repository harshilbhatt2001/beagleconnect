#!/bin/bash -ex
MYDIR=$(realpath $(dirname "${BASH_SOURCE[0]}"))
export PATH=$MYDIR/bcf-zephyr/zephyr/scripts:$PATH:$HOME/.local/bin

cd $MYDIR/bcf-zephyr
west init -l zephyr
west update
west zephyr-export
pip3 install -r zephyr/scripts/requirements-base.txt

export ZEPHYR_TOOLCHAIN_VAIRANT=cross-compile
export CROSS_COMPILE=/usr/bin/arm-none-eabi-
export ZEPHYR_BASE=$MYDIR/bcf-zephyr/zephyr
#export BOARD=beagleconnect_freedom

# Should this be moved to the manifest?
git clone https://github.com/zephyrproject-rtos/net-tools

# Build a demo here
west build -b native_posix_64 -d $MYDIR/build/greybus/net modules/lib/greybus/samples/subsys/greybus/net -p \
    -- -DOVERLAY_CONFIG="overlay-qemu_802154.conf overlay-log.conf overlay-debug.conf"

west build -b native_posix_64 -d $MYDIR/build/greybus/uart modules/lib/greybus/samples/subsys/greybus/uart -p \
    -- -DOVERLAY_CONFIG="overlay-log.conf overlay-debug.conf"

ls -lha $MYDIR/build/greybus/net/zephyr/
ls -lha $MYDIR/build/greybus/uart/zephyr/
